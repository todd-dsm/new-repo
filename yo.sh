#!/usr/bin/env bash
set -x

# Verify the ENV var; error with a question.
: "${USER? Wheres the USER, bro?!}"

# if no error, say hi
echo "Howdy, $USER!"
